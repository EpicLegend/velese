'use strict';
//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js

//= ../node_modules/popper.js/dist/umd/popper.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js


//= ../node_modules/slick-carousel/slick/slick.js
//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js



//= library/wow.js

//= library/jquery-ui.js


$(document).ready(function () {



	/* START  slider */
	//initialized

	/* END  slider */

	/* анимация блоков */
	new WOW().init({
		mobile: false
	});


	/* START preloader*/
	(function () {
		if ( Date.now() - preloader.timerStart >= preloader.duration  ) {
			$("#preloader-cube").animate({"opacity" : "0"}, preloader.duration / 2, function () {
				$("#preloader-cube").css("display", "none");
			});
		} else {
			setTimeout(function () {
				$("#preloader-cube").animate({"opacity" : "0"}, preloader.duration / 2, function () {
					$("#preloader-cube").css("display", "none");
				});
			}, preloader.duration / 1.7)
		}
	})();
	/* END preloader */


	if ( $(window).scrollTop() > 0 ) {
		$("header").addClass("scroll");
	}
	/* START Открытие меню */
	$(".btn__menu").on("click", function () {
		$(this).toggleClass("active");

		if ( $(this).hasClass("active") ) {
			$(".navigation__content").addClass("active");
			$(".navigation__content").css( "top", $(".header").outerHeight() );
			$("header").addClass("scroll");
		} else {
			$(".navigation__content").removeClass("active");
			console.log($(window).width());
			if ( ($(window).width() > 991)  && ($(window).scrollTop() <= 0) ) {
				$("header").removeClass("scroll");
			}
		}
	});

	$(".btn_header").on("click", function () {
		if( $(".navigation__content").hasClass("active") ) {
			$(".navigation__content").removeClass("active");
			$(".btn__menu").removeClass("active");
			$("#burger").removeClass("active");
		}
	});
	/* END откртие меню*/

	/* START красиво для hover в navigation */
	$(".navigation ul li a").hover(function () {
		var width = $(this).width();

		var elem = $(this);
		var offset = elem.offset().left - elem.parent().parent().offset().left;
		offset += parseInt( elem.css("padding-left") );

		$(".navigation__line").width( width );
		$(".navigation__line").css("left", offset);
		$(".navigation__line").css("bottom", "-6px");
	}, function () {
		// Забрать красоту! ВСЮ!
		$(".navigation__line").css("bottom", "-500%");
	});
	/* END красиво для hover в navigation */

	if ( $(window).scrollTop() > 0 ) {
        //$("header").addClass("bg-dark");
    }
	$(window).on("scroll", function () {
		if ( $(window).scrollTop() > 0 ) {
			$("header").addClass("scroll");
		}  else {
			$("header").removeClass("scroll");
		}

		/* START кнопка вверх */
		if ( $(window).scrollTop() > 100 ) {
			$(".btn-to-top").addClass("btn-to-top_active");
		} else {
			$(".btn-to-top").removeClass("btn-to-top_active");
		}

	});
	$('.btn-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });
	/* END кнопка вверх */










	$('#modalCalc').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) ;
		var recipient = button.data('whatever') ;
		//var modal = $(this);
		console.log(recipient);
		//modal.find('.modal-body .calc__val').val(recipient);
	})
	/* END calc event */

	$('.slick__partners').slick({
	  slidesToShow: 4,
	  slidesToScroll: 1,
	   arrows: false,
	   infinite: false,
	  speed: 300,
	  variableWidth: true,
	  autoplay: false,
	  dots: true,
	  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
	});
$('.slick__firstSite').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	   arrows: true,
	   infinite: false,
	  speed: 300,
	  autoplay: true,
	  dots: false,
	  responsive: [
    {
      breakpoint: 991,
      settings: {
        arrows: false,
        adaptiveHeight: true,
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
	});
$('.slick__excavator').slick({
	  slidesToShow: 5,
	  slidesToScroll: 5,
	   arrows: false,
	   infinite: false,
	  speed: 300,
	  autoplay: false,
	  dots: true,
	  responsive: [
	  {
      breakpoint: 1700,
      settings: {
      	slidesToShow: 4,
	  slidesToScroll: 4,
        arrows: false,
      }
    },
    {
      breakpoint: 1500,
      settings: {
      	slidesToShow: 3,
	  slidesToScroll: 3,
        arrows: false,
      }
    },
    {
      breakpoint: 991,
      settings: {
      	slidesToShow: 1,
	  slidesToScroll: 1,
        arrows: false,
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
	});



 $('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: false,
  arrows: false,
  centerMode: true,
  focusOnSelect: true
});


});


